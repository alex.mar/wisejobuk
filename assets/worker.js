// start the Stimulus application
import './app';

document.addEventListener('DOMContentLoaded', function () {
    let questions = document.querySelectorAll('.question');
    for (let i = 0; i < questions.length; i++) {
        questions[i].addEventListener('click', () => {
            let answer = questions[i].nextElementSibling;
            if (questions[i].classList.contains('active')) {
                questions[i].classList.remove('active');
                questions[i].querySelector('.icon-close').style.display = 'none';
                questions[i].querySelector('.icon-open').style.display = 'block';
                answer.classList.remove('active');
            } else {
                questions[i].classList.add('active');
                questions[i].querySelector('.icon-close').style.display = 'block';
                questions[i].querySelector('.icon-open').style.display = 'none';
                answer.classList.add('active');
            }
        });
    }
});