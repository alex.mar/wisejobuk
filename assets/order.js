// start the Stimulus application
import './app';

document.addEventListener('DOMContentLoaded', function () {
    let label = document.querySelector('.label-check');
    label.addEventListener('click', () => {
        let checkBox = label.previousElementSibling;
        checkBox.checked = checkBox.checked !== true;
    });

    let btnOrder = document.querySelector('.btn-order');
    btnOrder.addEventListener('click', formSend);

    async function formSend(e)
    {
        e.preventDefault();
        let formOrder = document.getElementById('form-order');
        let formData = new FormData(formOrder);
        formData.append('submit', JSON.stringify(true));
        let formInputs = document.querySelectorAll('.form__input');
        let hasEmpty = false;
        for (let i = 0; i < formInputs.length; i++) {
            if (formInputs[i].value.trim().length === 0) {
                hasEmpty = true;
                formInputs[i].nextElementSibling.style.display = 'block';
            } else {
                formInputs[i].nextElementSibling.style.display = 'none';
            }
        }
        let checkboxAgree = document.querySelector('.checkbox-agree');
        let checkboxError = document.querySelector('.checkbox-agree-error');
        if (checkboxAgree.checked === false) {
            hasEmpty = true;
            checkboxError.style.display = 'block';
        } else {
            checkboxError.style.display = 'none';
        }

        if (hasEmpty) {
            return false;
        }

        enableLoadingBtn(btnOrder);
        let response = await fetch('/order/', {
            method: 'POST',
            body: formData
        });
        if (response.ok) {
            disableLoadingBtn(btnOrder);
            let result = await response.json();
            let status = result.status;
            if (status === 'success') {
                let successNotice = document.querySelector('.item-success');
                successNotice.style.display = 'block';
                // await new Promise(r => setTimeout(r, 3000));
                // window.location.reload();
            } else if (status === 'error') {
                alert('Произошла ошибка! Попробуйте позже');
            }
        } else {
            alert('Произошла ошибка! Попробуйте позже');
        }

        return false;
    }

    function enableLoadingBtn(btn) {
        btn.style.backgroundColor = '#f36161b5';
        btn.style.cursor = 'auto';
        btn.disabled = true;
        let preloader = btn.nextElementSibling;
        preloader.style.visibility = 'visible';
    }

    function disableLoadingBtn(btn) {
        btn.style.backgroundColor = '#f36161';
        btn.style.cursor = 'pointer';
        btn.disabled = false;
        let preloader = btn.nextElementSibling;
        preloader.style.visibility = 'hidden';
    }
});