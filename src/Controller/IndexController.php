<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\OrderEntity;
use App\Repository\OrderRepository;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        return $this->render('base.html.twig');
    }

    #[Route('/worker/', name: 'app_worker')]
    public function worker(): Response
    {
        return $this->render('worker.html.twig');
    }

    #[Route('/order/', name: 'app_order')]
    public function order(
        Request $request,
        OrderRepository $orderRepository,
        ParameterBagInterface $params
    ): Response {
        $submit = (bool) $request->get('submit');
        if ($submit) {
            $name = strip_tags($request->get('name'));
            $surname = strip_tags($request->get('surname'));
            $email = strip_tags($request->get('email'));
            $phone = strip_tags($request->get('phone'));
            $nationality = strip_tags($request->get('nationality'));
            $location = strip_tags($request->get('location'));
            $language = strip_tags($request->get('language'));

            try {
                $entities = $orderRepository->findBy(['name' => $name, 'surname' => $surname, 'email' => $email, 'phone' => $phone]);
                if (empty($entities)) {
                    $order = new OrderEntity();
                    $order->setName($name);
                    $order->setSurname($surname);
                    $order->setEmail($email);
                    $order->setPhone($phone);
                    $order->setNationality($nationality);
                    $order->setLocation($location);
                    $order->setLanguage($language);

                    $orderRepository->add($order, true);

                    $transport = new Swift_SmtpTransport(
                        $params->get('mailer_host'),
                        $params->get('mailer_port'),
                        $params->get('mailer_encryption'),
                    );
                    $transport
                        ->setAuthMode($params->get('mailer_auth_mode'))
                        ->setUsername($params->get('mailer_username_no_reply'))
                        ->setPassword($params->get('mailer_password_no_reply'))
                        ->setStreamOptions(
                            ['ssl' => ['allow_self_signed' => true, 'verify_peer' => false, 'verify_peer_name' => false]]
                        );

                    $mailer = new Swift_Mailer($transport);
                    $mailerAdmins = $params->get('mailer_admins');
                    $mailerAdmins = explode(',', $mailerAdmins);
                    foreach ($mailerAdmins as $adminMail) {
                        $message = (new Swift_Message('Новая заявка'))
                            ->setFrom([$params->get('mailer_username_no_reply') => 'WISEJOB'])
                            ->setTo($adminMail)
                            ->setBody($this->renderView('email.html.twig', [
                                'name' => $name,
                                'surname' => $surname,
                                'email' => $email,
                                'phone' => $phone,
                                'nationality' => $nationality,
                                'location' => $location,
                                'language' => $language,
                            ]), 'text/html');

                        $mailer->send($message);
                    }
                }
                $status = 'success';
            } catch (\Exception $e) {
                $status = 'error';
            }

            return new JsonResponse(['status' => $status]);
        }
        return $this->render('order.html.twig');
    }
}
